package backend

import (
	"time"

	"github.com/google/uuid"
)

type Person struct {
	ID        uuid.UUID  `json:"id"`
	BSN       string     `json:"bsn,omitempty"`
	Name      string     `json:"name,omitempty"`
	BornAt    *time.Time `json:"bornAt,omitempty"`
	Sex       string     `json:"sex,omitempty"`
	DiedAt    *time.Time `json:"diedAt,omitempty"`
	AddressID *uuid.UUID `json:"registeredAddressId,omitempty"`
	Partners  []Person   `json:"partners,omitempty"`
	Children  []Person   `json:"children,omitempty"`
	Parents   []Person   `json:"parents,omitempty"`
}

type PersonPatch struct {
	BSN    *string    `json:"bsn"`
	Name   *string    `json:"name"`
	BornAt *time.Time `json:"bornAt"`
	Sex    *string    `json:"sex"`
	DiedAt *time.Time `json:"diedAt"`
}

type Marry struct {
	Person1 uuid.UUID `json:"person1"`
	Person2 uuid.UUID `json:"person2"`
}

type AddChild struct {
	Person uuid.UUID `json:"person"`
}

type Pagination struct {
	FirstID *uuid.UUID `json:"firstId"`
	LastID  *uuid.UUID `json:"lastId"`
	PerPage int        `json:"perPage"`
}

type Response[T any] struct {
	Data       []T         `json:"data"`
	Pagination *Pagination `json:"pagination"`
}
