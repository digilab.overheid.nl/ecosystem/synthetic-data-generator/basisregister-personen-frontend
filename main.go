package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/url"
	"text/template"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-frontend/backend"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-frontend/helpers"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(backend.Response[backend.Person])
		if err := backend.Request("GET", fmt.Sprintf("/people?%s", query.Encode()), nil, &response); err != nil {
			return fmt.Errorf("error fetching people: %v", err)
		}

		return c.Render("index", fiber.Map{"people": response.Data, "pagination": response.Pagination})
	})

	people := app.Group("/people")

	// Endpoints to manually add test data. Note: might be unused in the future, since this is not part of the project's scope
	people.Get("/new", func(c *fiber.Ctx) error {
		return c.Render("person-new", nil)
	})

	people.Post("/", func(c *fiber.Ctx) error {
		return errors.New("not implemented")

		// p := new(struct{
		// 	ID       uuid.UUID  `json:"id"`
		// 	BSN      string     `json:"bsn,omitempty"`
		// 	Name     string     `json:"name,omitempty"`
		// 	BornAt   *time.Time `json:"bornAt,omitempty"`
		// 	Sex      string     `json:"sex,omitempty"`
		// 	DiedAt   *time.Time `json:"diedAt,omitempty"`
		// })

		// // Decode the request body
		// if err := c.BodyParser(p); err != nil {
		// 	return err
		// }

		// // Send the request to the backend
		// var data map[string]interface{}
		// if err := backend.Request("POST", "/people", p, &data); err != nil {
		// 	return err
		// }

		// return nil
	})

	// Route to view person details
	people.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the person from the backend
		var person backend.Person
		if err := backend.Request("GET", fmt.Sprintf("/people/%s", c.Params("id")), nil, &person); err != nil {
			return fmt.Errorf("error fetching person: %v", err)
		}

		return c.Render("person-details", fiber.Map{"person": person})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
